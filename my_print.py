#!/bin/usr/python3

def print_chance(g_chance: float, g_comb: (str, int, int)):
    if len(g_comb) == 2:
        if g_comb[0] == "four" or g_comb[0] == "three":
            print(f"Chances to get a {g_comb[1]} {g_comb[0]}-of-a-kind: {g_chance:.02f}%")
        else:
            print(f"Chances to get a {g_comb[1]} {g_comb[0]}: {g_chance:.02f}%")
    else:
        print(f"Chances to get a {g_comb[1]} {g_comb[0]} of {g_comb[2]}: {g_chance:.02f}%")


def print_helper(g_program: str):
    print(f"USAGE\n\t{g_program} d1 d2 d3 d4 d5 c\n")
    print("DESCRIPTION")
    print("\td1\tvalue of the first die(0 if not thrown)")
    print("\td2\tvalue of the second die(0 if not thrown)")
    print("\td3\tvalue of the third die(0 if not thrown)")
    print("\td4\tvalue of the fourth die(0 if not thrown)")
    print("\td5\tvalue of the fifth die(0 if not thrown)")
    print("\tc\texpected combination")
