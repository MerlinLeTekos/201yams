#!/bin/usr/python3

exp_comb = [
    "pair",
    "three",
    "four",
    "full",
    "straight",
    "yams"
]


def error_hangling(av):
    if len(av) != 7:
        print("Not enough argument")
        exit(84)


def check_dice(av) -> [int]:
    prev = -1
    result = []
    for i in range(1, 6):
        try:
            x = int(av[i])
            if prev != -1 and prev == 0 and x != 0:
                raise Exception
            prev = x
            if not 0 <= x <= 6:
                raise Exception
            result.append(x)
        except:
            print("Wrong dice argument")
            exit(84)
    return result


def check_comb(combination) -> (str, int, int):
    try:
        if "_" not in combination:
            raise Exception
        buf = combination.split("_")
        if buf[0] not in exp_comb:
            raise Exception
        if buf[0] == "straight" and (int(buf[1]) != 6 and int(buf[1]) != 5):
            raise Exception
        if len(buf) == 3 and buf[0] == "full":
            if 0 < int(buf[1]) <= 6 and 0 < int(buf[2]) <= 6 and int(buf[1]) != int(buf[2]):
                return buf[0], int(buf[1]), int(buf[2])
            else:
                raise Exception
        elif len(buf) >= 3 or buf[0] == "full":
            raise Exception
        if 0 < int(buf[1]) <= 6:
            return buf[0], int(buf[1])
        else:
            raise Exception
    except:
        print("Non valide combinaison")
        exit(84)
