#!/bin/usr/python3

import math


def binomiale(a, b):
    res = (math.factorial(a) / (math.factorial(b) * math.factorial(a-b))) * pow(1/6, b) * pow(5/6, a-b)
    return res


def calcProbabilite(g_nbDice: int, g_nb: int, g_dice: [int]) -> float:
    res = 0
    occu_dice = count_dice(g_dice, g_nb)
    if occu_dice >= g_nbDice:
        res = 1
    else:
        for x in range(g_nbDice - occu_dice, 5 - occu_dice + 1):
            res += binomiale(5 - occu_dice, x)
    return res


# Return the number of good dice
def count_dice(g_dice: [int], g_nb: int):
    i = 0
    for die in g_dice:
        if die == g_nb:
            i += 1
    return i


def pair(g_dice, g_nb) -> float:
    return calcProbabilite(2, g_nb, g_dice) * 100


def three(g_dice, g_nb) -> float:
    return calcProbabilite(3, g_nb, g_dice) * 100


def four(g_dice, g_nb) -> float:
    return calcProbabilite(4, g_nb, g_dice) * 100

def full(g_dice, g_nb1, g_nb2) -> float:
    result = 1.0
    apparition1 = count_dice(g_dice, g_nb1)
    apparition2 = count_dice(g_dice, g_nb2)
    if (apparition1 > 3):
        apparition1 = 3
    if (apparition2 > 2):
        apparition2 = 2
    for n in range(5 - apparition1 - apparition2, 6 - apparition1 - apparition2):
        result *= binomiale(5 - apparition1 - apparition2, n)
    if result < 0.001:
        result *= 10
    return result * 100


def straight(g_dice, g_nb) -> float:
    which = 6 if (g_nb == 6) else 1
    here = [0, 0, 0, 0, 0]
    res = 0
    startof = 0
    for die in g_dice:
        if int(die) == 2 and here[0] == 0:
            here[0] = 1
        elif int(die) == 3 and here[1] == 0:
            here[1] = 1
        elif int(die) == 4 and here[2] == 0:
            here[2] = 1
        elif int(die) == 5 and here[3] == 0:
            here[3] = 1
        elif int(die) == 6 and which == 6 and here[4] == 0:
            here[4] += 1
        elif int(die) == 1 and which == 1 and here[4] == 0:
            here[4] += 1
    res = here[0] + here[1] + here[2] + here[3] + here[4]
    res = math.factorial(5 - res) / math.pow(6, (5 - res))
    return res * 100


def yams(g_dice, g_nb) -> float:
    return calcProbabilite(5, g_nb, g_dice) * 100